

# Getting Started 

After cloning the project, please run an npm install to download all the dependencies.

For simplicity, I removed the .env in the .gitignore and pushed the .env as well. Please feel free to modify the keys for the openweather.

I also used the api https://api.mapbox.com/geocoding/v5/mapbox.places/ for getting the coordinates of the city and other information like country and state.

Please feel free to register at https://api.mapbox.com if you prefer to use your own key.

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### The website

In the website, there are three default tiles which would be Berlin, New York and London.

Please search in the serch box to look for other cities to add and then press enter to serach.

A preview of the data will be shown and to add the data to the tiles list:

   1) Choose an interval
   2) Press Add Dashboard

In the website, you can also press the remove button to remove a tile.

In the tile, there are extra details:

   1) Interval - The chosen interval when adding. This can be modified. Once modified, it will be the new interval automatically.
   2) Updated as at - This is the date when the tile is last updated.

## Contact
Please feel free to conteact me if you find any issues. [johnrobert_bgarcia@yahoo.com](mailto:johnrobert_bgarcia@yahoo.com)
