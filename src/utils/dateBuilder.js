export const dateBuilder = (dateNow, withDay = true) => {
    const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    const days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

    const day = days[dateNow.getDay()];
    const date = dateNow.getDate();
    const month = months[dateNow.getMonth()];
    const year = dateNow.getFullYear();
    
    return `${withDay ? `${day} ` : ''}${date} ${month} ${year}`;
  }