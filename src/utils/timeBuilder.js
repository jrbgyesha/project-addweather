export const timeBuilder = (dateNow) => {
    var seconds = dateNow.getSeconds();
    var minutes = dateNow.getMinutes();
    var hour = dateNow.getHours();

    seconds = seconds < 10 ? `0${seconds}` : seconds;
    minutes = minutes < 10 ? `0${minutes}` : minutes;
    hour = hour < 10 ? `0${hour}` : hour;
    return `${hour}:${minutes}:${seconds}`;
  }