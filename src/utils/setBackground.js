export const setBackground = () => {
    const now = new Date();
    const hour = now.getHours();
    if (hour <= 7 || hour > 18) {
        return 'blue';
    }
    if (hour > 7 && hour <= 15) {
        return 'green';
    }
    return 'orange';
}