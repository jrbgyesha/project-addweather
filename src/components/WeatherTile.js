import React, { useState, useEffect } from 'react';
import StyledWeatherTile from './styles/StyledWeatherTile';

import { dateBuilder } from '../utils/dateBuilder';
import { timeBuilder } from '../utils/timeBuilder';

const WeatherTile = ({ weatherData, events: {fetchData, RemoveWeatherData} }) => {
    if (weatherData && weatherData.location && weatherData.weather && weatherData.interval) {
        const [weatherTileData, setweatherTileData] = useState(weatherData);
        const [currentDateNoDay, setCurrentDateNoDay] = useState('');
        const [currentTime, setCurrentTime] = useState('');
        const [selectInterval, setSelectInterval] = useState(weatherData.interval || 30);

        const handleIntervalChange = (e) => {
            setSelectInterval(e.target.value)
        }

        const reloadWeather = () =>{
            fetchData(weatherData.location.city).then(([weather, location]) => {
                setweatherTileData({key: weatherData.key, location, weather, interval: selectInterval });
            });
        }

        useEffect(() => {
            let now = new Date();
            setCurrentTime(timeBuilder(now))
            setCurrentDateNoDay(dateBuilder(now, false));
            if (selectInterval && selectInterval > 0) {
                const interval = setInterval(() => { reloadWeather(); }, (selectInterval *  1000));
                return () => clearInterval(interval);
            }
        });

        return (
            <StyledWeatherTile key={weatherTileData.key}>
                <div className="col-4">
                    <div className="card">
                        <div className="card-body">
                            <h5 className="card-title">{weatherTileData.location.city}</h5>
                            <h6 className="card-subtitle mb-2 text-muted">{weatherTileData.location.state}, {weatherTileData.location.country}</h6>
                            <hr />
                            <div className="row temp">
                                <h2>{Math.round(weatherTileData.weather.currentTemp)}<span>&#186;C</span></h2>
                            </div>
                            <div className="row weatherMain">
                                <h2>{weatherTileData.weather.weatherMain}</h2>
                            </div>
                            <div className="row tempMinMax">
                                <div className="minmax">
                                    <h5>{Math.round(weatherTileData.weather.tempMin)}&#186;C / {Math.round(weatherTileData.weather.tempMax)}&#186;C</h5>
                                </div>
                            </div>
                            <hr className="bottomHr" />
                            <button className="btn" onClick={() => {RemoveWeatherData(weatherTileData.key)}}>Remove</button>
                            <div className="intervalFooter">
                                interval:
                            </div>
                            <div className="intervalFooterSelect">
                                <select defaultValue={selectInterval} className="form-select form-select-sm selectInterval" aria-label=".form-select-sm" onChange={handleIntervalChange}>
                                    <option value="5">5 s</option>
                                    <option value="15">15 s</option>
                                    <option value="30">30 s</option>
                                    <option value="60">60 s</option>
                                    <option value="75">75 s</option>
                                </select>
                            </div>
                            <div className="asAtUpdatedLabel">
                                Updated as at
                            </div>
                            <div className="asAtUpdatedDate">
                                {currentDateNoDay}
                            </div>
                            <div className="asAtUpdatedTime">
                                {currentTime}
                            </div>
                        </div>
                    </div>
                </div>
            </StyledWeatherTile>
        )
    } else {
        return "";
    }
};

export default WeatherTile;