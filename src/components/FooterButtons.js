import React from 'react';

import StyledFooterButtons from './styles/StyledFooterButtons';

const FooterButtons = ({events: {AddDashboard, handleIntervalChange}, interval}) => {
return (
  <StyledFooterButtons>
    <div className="row">
        <div className="col-5 label">
            Select Interval: 
        </div>
       <div className="col-2 interval">
         <select defaultValue={interval} className="form-select form-select-sm selectInterval" aria-label=".form-select-sm" onChange={handleIntervalChange}>
           <option value="5">5 s</option>
           <option value="15">15 s</option>
           <option value="30">30 s</option>
           <option value="60">60 s</option>
           <option value="75">75 s</option>
         </select>
       </div>
       <div className="col-5">
         <button className="btn addDashboard" onClick={(e) => (AddDashboard(e))}>Add Dashboard</button>
       </div>
    </div>
  </StyledFooterButtons>
);
}

export default FooterButtons;