import styled from 'styled-components';

const StyledWeatherList = styled.div`
  color: #000;
  padding: 10px;

  & div.card {
    width: 300px;
	opacity: 0.7;
  }

  & div.temp, div.weatherMain  {
      font-weigth: bold;
      font-style: italic;
      text-shadow: 0px 2px rgba( 0, 0, 0, 0.10);
      display: flex;
      flex-direction: column;
      align-items: center;
      text-align: center;
  }

  & div.tempMinMax {
    flex-direction: column;
    align-items: center;
    text-align: center;
      font-weigth: bold;
      text-shadow: 0px 2px rgba( 0, 0, 0, 0.10);
  }

  & h5 {
      color: black !important;
  }

  & button {
      font-size: 12px;
      padding: 3px;
      &:hover {
        border-color: grey;
      }
  }

  & div.intervalFooter {
      font-size: 11px;
      position: absolute;
      left: 2%;
      bottom: 2%;
  }

  & hr.bottomHr {
      padding-bottom: 0;
      margin-bottom: 10px;
  }
`

export default StyledWeatherList;