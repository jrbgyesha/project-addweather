import styled from 'styled-components';

const StyledWeatherTile = styled.div`
  & div.card {
    width: 300px;
	opacity: 0.7;
  }

  & div.temp, div.weatherMain  {
      font-weigth: bold;
      font-style: italic;
      text-shadow: 0px 2px rgba( 0, 0, 0, 0.10);
      display: flex;
      flex-direction: column;
      align-items: center;
      text-align: center;
  }

  & div.tempMinMax {
    flex-direction: column;
    align-items: center;
    text-align: center;
      font-weigth: bold;
      text-shadow: 0px 2px rgba( 0, 0, 0, 0.10);
  }

  & h5 {
      color: black !important;
  }

  & button {
      font-size: 12px;
      padding: 3px;
      &:hover {
        border-color: grey;
      }
  }

  & div.intervalFooter, div.intervalFooterSelect {
      font-size: 11px;
      position: absolute;
      bottom: 2%;
  }

  & div.intervalFooter, {
    left: 2%;
  }

  & div.intervalFooterSelect {
    left: 16%;
    border: none !important;
  }

  & div.intervalFooterSelect select {
    border: 0px solid white;
    &:hover {
      border-bottom: 1px solid grey;
      cursor: pointer
    }
  }

  & div.asAtUpdatedLabel, div.asAtUpdatedDate, div.asAtUpdatedTime {
      font-size: 11px;
      position: absolute;
      right: 2%;
  }

  & div.asAtUpdatedLabel {
    font-weight: bold;
    bottom: 9%;
  }

  & div.asAtUpdatedDate {
    bottom: 5%;
  }

  & div.asAtUpdatedTime {
    bottom: 1%;
  }

  & hr.bottomHr {
      padding-bottom: 0;
      margin-bottom: 10px;
  }
`

export default StyledWeatherTile;