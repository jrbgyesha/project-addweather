import styled from 'styled-components';

const StyledFooterButtons = styled.div`
  margin-top: 20px;

  & button.addDashboard {
    box-shadow: 2px 3px 15px grey;
    background-color: white;
    color: black;
    padding: 3px;
    &:hover {
      box-shadow: 2px 3px 15px black;
      color: black;
    }
  }

  & select.selectInterval{
    margin-top 3px;
    padding: 2px;
    cursor: pointer;
    border-radius: 2px;
  }

  & .label {
      color: white;
      margin-top: 5px;
      text-align: right;
  }

  & .interval {
    margin-left: -20px;
    margin-right: 20px;
  }
  `

export default StyledFooterButtons;