import React from 'react';

import StyledWeatherList from './styles/StyledWeatherList';
import WeatherTile from './WeatherTile';

const WeatherList = ( { weatherList, events } ) => {
    return(
        <div className="row">
            {weatherList && Array.isArray(weatherList) ? weatherList.map(w =>
            <StyledWeatherList key={w.key}>
                <WeatherTile weatherData={w} events={events}/>
            </StyledWeatherList>
            ) : ""}
        </div>
    )
};

export default WeatherList;