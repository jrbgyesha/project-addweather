import React, { useState, useEffect } from 'react';
import Noty from 'noty';  
import "../../node_modules/noty/lib/noty.css";  
import "../../node_modules/noty/lib/themes/mint.css";  

import Wrapper from './Wrapper';

import { getWeather } from '../utils/fetchHelpers';
import { dateBuilder } from '../utils/dateBuilder';
import { setBackground } from '../utils/setBackground';

import StyledWeather from './styles/StyledWeather';

const Weather = () => {
  const [inputLocation, setInputLocation] = useState('');
  const [currentDate, setCurrentDate] = useState('');
  const [isTest, setIsTest] = useState(false);
  const [location, setLocation] = useState({
    city: '',
    state: '',
    country: ''
  });
  const [weather, setWeather] = useState({
    currentTemp: 0,
    weatherMain: '',
    tempMax: 0,
    tempMin: 0
  });

  const [interval, setInterval] = useState(15);

  const [weatherList, setWeatherList] = useState([]);

  useEffect(() => {
    fetchWeather('Berlin', 15)
    fetchWeather('New York', 30)
    fetchWeather('London', 60)
  }, []);

  const fetchWeather = (city, paramInterval = 15) =>{
    fetchData(city).then(([newWeather, placeName]) => {
      setWeather(newWeather);
      setLocation(placeName);
      setWeatherList(state => {
        if (state.length === 0 || state.filter(s => s.location.city === placeName.city && s.location.country === placeName.country && s.location.state === placeName.state).length === 0)
          return [{ key: `weatherList-${state.length+1}`, location: placeName, weather: newWeather, interval: paramInterval ? paramInterval : interval }, ...state];
        else
          return state;
      });
    });
  }

  const RemoveWeatherData = (key) => {
    setWeatherList(state => {
      if (state.length > 0 || state.filter(w => w.key === key).length > 0)
        return state.filter(w => w.key !== key);
      else
        return state;
    });
  }

  async function fetchData(newLocation) {
    const now = new Date();
    setCurrentDate(dateBuilder(now));
    const response = await getWeather(newLocation);
    return response;
  }

  const handleInputLocation = (e) => {
    e.preventDefault();
    setInputLocation(e.target.value);
  }

  const getForecast = (e) => {
    e.preventDefault();
    fetchData(inputLocation).then(([newWeather, placeName]) => {
      setWeather(newWeather);
      setLocation(placeName);
    });
  }

  const AddDashboard = (e) =>{
    e.preventDefault();
    setWeatherList(state => {
      if (state.length === 0 || state.filter(s => s.location.city === location.city && s.location.country === location.country && s.location.state === location.state).length === 0)
        return [{ key: `weatherList-${state.length+1}`, location, weather, interval}, ...state];
      else{
        new Noty({
          text: `${location.city} is already added.`,
          theme: "mint",
          type: "error",
          timeout: 5000,
        }).show();

        return state
      }
    });
  }

  const handleIntervalChange = (e) => {
    setInterval(e.target.value)
  }

  return (
    <StyledWeather bgImage={setBackground()}>
      <Wrapper states={{ location, currentDate, weather, weatherList, interval }} handleInput={handleInputLocation} handleSubmit={getForecast} events={{AddDashboard, handleIntervalChange, fetchData, RemoveWeatherData}} />
    </StyledWeather>
  );
}

export default Weather;