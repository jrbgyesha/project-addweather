import React from 'react';

import SearchBox from './SearchBox';
import Geolocation from './Geolocation';
import WeatherDisplay from './WeatherDisplay';
import WeatherList from './WeatherList';
import FooterButtons from './FooterButtons';
import StyledWrapper from './styles/StyledWrapper';

const Wrapper = ({ handleInput, handleSubmit, states, events }) => 
{
 return (
  <StyledWrapper>
    <h3 className="dateHeader"> {states.currentDate} </h3>
    <WeatherList weatherList={states.weatherList} interval={states.interval} events={events} />
    <SearchBox handleInput={handleInput} handleSubmit={handleSubmit} />
    <Geolocation location={states.location} currentDate={states.currentDate} />
    <WeatherDisplay weather={states.weather} />
    <FooterButtons events={events} interval={states.interval}></FooterButtons>
  </StyledWrapper>
);
}


export default Wrapper;